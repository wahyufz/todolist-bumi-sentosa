import {Component} from '@angular/core';
import { Todo } from '../models/todo.interface';

const ELEMENT_DATA: Todo[] = [
  {name: 'Neon', description: 'hohohoho',created_by:'demo'},
  {name: 'nini', description: 'askdjfh',created_by:'demo'},
  {name: 'nan', description: 'kajds',created_by:'demo'},
  {name: 'kasjdn', description: 'lakjdshf',created_by:'demo'},
  {name: 'aksjd', description: 'lkajdhf',created_by:'ad'},
];
@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  displayedColumns: string[] = [ 'name', 'description', 'created_by'];
  dataSource = ELEMENT_DATA;
}
