export interface Todo {
  name: string;
  description: string;
  created_by: string;
}
