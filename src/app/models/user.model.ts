export class User {
  id: number;
  company_id: number;
  role_id: number;
  username: string;
  email: string;
}
