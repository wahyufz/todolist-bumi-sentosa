import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { User } from "./models/user.model";

@Injectable({
  providedIn: "root"
})
export class JwtService {
  constructor(private httpClient: HttpClient) {}

  private headers = {
    headers: new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded"
    })
  };

  private loginAPI = "https://api.smartbiz.id/api/login";

  login(user) {
    return this.httpClient.post<any>(
      `${this.loginAPI}?identity=${user.identity}&password=${user.password}`,
      null,
      this.headers
    );
  }

  logout() {
    return localStorage.removeItem("currentUser");
    // this.currentUserSubject.next(null);
  }

  public get loggedIn(): boolean {
    return localStorage.getItem("access_token") !== null;
  }
}
