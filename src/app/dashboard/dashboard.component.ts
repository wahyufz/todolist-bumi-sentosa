import { Component, OnInit } from '@angular/core';
import { JwtService } from '../jwt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _auth:JwtService,private router:Router) { }

  ngOnInit() {
    if (!this._auth.loggedIn) this.router.navigate(["/login"]);
  }

}
