import { BrowserModule } from "@angular/platform-browser";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { NgModule } from "@angular/core";

import { CustomMaterialModule } from "./core/material.module";

import { AppRoutingModule } from "./core/app.routing.module";

import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";

import { LoginComponent } from "./login/login.component";

import { DashboardComponent } from "./dashboard/dashboard.component";
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { TodoComponent } from './todo/todo.component';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, DashboardComponent, TodoComponent, LogoutComponent],

  imports: [
    BrowserModule,

    HttpClientModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: function  tokenGetter() {
             return     localStorage.getItem('access_token');},
        whitelistedDomains: ['localhost:4200'],
        blacklistedRoutes: ['http://localhost:4200/auth/login']
      }
    }),

    BrowserAnimationsModule,

    CustomMaterialModule,

    FormsModule,

    AppRoutingModule
  ],

  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule {}
