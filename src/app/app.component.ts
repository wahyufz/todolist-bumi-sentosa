import { Component, OnInit } from '@angular/core';
import { JwtService } from './jwt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private _auth:JwtService, private router:Router) { }
  title = 'To Do List';
  isLoggedIn:boolean = false

  ngOnInit(){
      this.isLoggedIn = this._auth.loggedIn
  }
}
