import { Component, OnInit } from "@angular/core";

import { Router } from "@angular/router";

import { MatDialog } from "@angular/material";

import { JwtService } from "../jwt.service";
import { JsonPipe } from "@angular/common";

@Component({
  selector: "app-login",

  templateUrl: "./login.component.html",

  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginUserData = {};

  formError = {
    identity: "",
    password: ""
  };

  constructor(private _auth: JwtService, private router: Router) {}

  ngOnInit() {
    if (this._auth.loggedIn) this.router.navigate(["/"]);
  }

  login() {
    this._auth.login(this.loginUserData).subscribe(
      res => {
        console.log(res);
        if (res.isSuccess) {
          if (res.value.token.message) {
            this.formError.password = "Password and Username not match";
          } else {
            this.formError.identity = "";
            this.formError.password = "";
            localStorage.setItem(
              "access_token",
              JSON.stringify(res.value.token.access_token)
            );
            this.router.navigate(["/"]);
          }
        } else this.formError.identity = "Username not found";
      },
      err => console.log(err)
    );
  }
}
