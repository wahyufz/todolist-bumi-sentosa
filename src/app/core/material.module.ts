import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';

import {

  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,

  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule, MatPaginatorModule

} from '@angular/material';

@NgModule({

  imports: [

  CommonModule,

  MatToolbarModule,

  MatButtonModule,

  MatPaginatorModule,

  MatCardModule,

  MatInputModule,

  MatDialogModule,

  MatTableModule,

  MatMenuModule,

  MatIconModule,

  MatProgressSpinnerModule

  ],

  exports: [

  CommonModule,

   MatToolbarModule,

   MatButtonModule,

   MatCardModule,

   MatPaginatorModule,

   MatInputModule,

   MatDialogModule,

   MatTableModule,

   MatMenuModule,

   MatIconModule,

   MatProgressSpinnerModule

   ],

})

export class CustomMaterialModule { }
